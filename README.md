# Slash GraphQL (with Angular) Spring Boot Example Application 

> The Slash GraphQL (with Angular) Spring Boot Example Application (`slash-graph-ql-angular`) is a full-stack [Java](<https://en.wikipedia.org/wiki/Java_(programming_language)>) API service (utilizing the [Spring Boot](<https://spring.io/projects/spring-boot>) framework) and [Angular](https://angular.io/) client application.  This repository is intended to provide a working example of [Slash GraphQL](https://dgraph.io/slash-graphql) with both a simple client and a corresponding API.  There is an in-memory [H2](https://www.h2database.com/html/main.html) database running as well, which is integrated with the Slash GraphQL results data.

> In this example the [Dgraph Slash GraphQL](https://dgraph.io/slash-graphql) service is utilized to populate data into a [Slope One](https://en.wikipedia.org/wiki/Slope_One) ratings algorithm to act as a recommendation engine - based upon data found in Dgraph Slash GraphQL.  That information is presented within a standard Angular application.

## Publications

This repository is related to a two-part series published on DZone.com:

* Part #1 - [Building an Amazon-Like Recommendation Engine Using Slash GraphQL](https://dzone.com/articles/building-a-recommendations-engine-using-spring-boo)
* Part #2 - [Connecting Angular to the Spring Boot and Slash GraphQL Recommendations Engine](https://dzone.com/articles/connecting-angular-to-the-spring-boot-and-slash-gr)

## Slash GraphQL Configuration

After creating a free Dgraph Slash GraphQL account, the following schema needs to be created:

```
type Artist {
    name: String! @id @search(by: [hash, regexp])
    ratings: [Rating] @hasInverse(field: about)
}

type Customer {
    username: String! @id @search(by: [hash, regexp])
    ratings: [Rating] @hasInverse(field: by)
}

type Rating {
    id: ID!
    about: Artist!
    by: Customer!
    score: Int @search
}
```

With the schema in place, a few example `Artist`s can be added using the API Explorer in Dgraph GraphQL:

```
mutation {
  addArtist(input: [
    {name: "Eric Johnson"},
    {name: "Genesis"},
    {name: "Journey"},
    {name: "Led Zeppelin"},
    {name: "Night Ranger"},
    {name: "Rush"},
    {name: "Tool"},
    {name: "Triumph"},
    {name: "Van Halen"},
    {name: "Yes"}]) {
    artist {
      name
    }
  }
}
```

Next, a base of example `Customer`s are added using the API Explorer in Dgraph GraphQL:

```
mutation {
  addCustomer(input: [
    {username: "David"},
    {username: "Doug"},
    {username: "Jeff"},
    {username: "John"},
    {username: "Russell"},
    {username: "Stacy"}]) {
    customer {
      username
    }
  }

```

Finally, some `Rating`s data needs to be added into the rating object, using the Dgraph GraphQL API Explorer:

```
mutation {
  addRating(input: [{
    by: {username: "Jeff"},
    about: { name: "Triumph"},
    score: 4}])
  {
    rating {
      score
      by { username }
      about { name }
    }
  }
}
```

For this example, I populated the data as shown below:

![Example Matrix (v2)](./ExampleRatingsMatrixV2.png)

## H2 Database Information

The H2 database will provide additional data for the `Artist`s that are captured in Slash GraphQL.  The following SQL statements are executed each time the Spring Boot service starts:

```
INSERT INTO Artists (name, year_formed, active, image_url) VALUES ('Eric Johnson', '1969', true, 'https://upload.wikimedia.org/wikipedia/commons/thumb/7/7e/Eric_Johnson_2017_%C2%A9John_Bland.jpg/220px-Eric_Johnson_2017_%C2%A9John_Bland.jpg');
INSERT INTO Artists (name, year_formed, active, image_url) VALUES ('Genesis', '1967', true, 'https://upload.wikimedia.org/wikipedia/commons/thumb/e/e4/2180_-_Pittsburgh_-_Mellon_Arena_-_Genesis_-_The_Carpet_Crawlers.JPG/1280px-2180_-_Pittsburgh_-_Mellon_Arena_-_Genesis_-_The_Carpet_Crawlers.JPG');
INSERT INTO Artists (name, year_formed, active, image_url) VALUES ('Journey', '1973', true, 'https://upload.wikimedia.org/wikipedia/commons/thumb/b/be/Journey_publicity_photo_2013.jpg/220px-Journey_publicity_photo_2013.jpg');
INSERT INTO Artists (name, year_formed, active, image_url) VALUES ('Led Zeppelin', '1968', false, 'https://upload.wikimedia.org/wikipedia/commons/thumb/4/49/LedZeppelinmontage.jpg/220px-LedZeppelinmontage.jpg');
INSERT INTO Artists (name, year_formed, active, image_url) VALUES ('Night Ranger', '1982', true, 'https://upload.wikimedia.org/wikipedia/commons/thumb/1/1d/Night_Ranger_live.jpg/300px-Night_Ranger_live.jpg');
INSERT INTO Artists (name, year_formed, active, image_url) VALUES ('Rush', '1968', false, 'https://upload.wikimedia.org/wikipedia/commons/thumb/4/46/Rush-in-concert.jpg/267px-Rush-in-concert.jpg');
INSERT INTO Artists (name, year_formed, active, image_url) VALUES ('Tool', '1990', false, 'https://cdn.theatlantic.com/thumbor/CVhzvTEf3hJ3e_ozh3IPMVdwcYg=/0x200:1920x1280/720x405/media/img/mt/2019/08/unnamed_16/original.jpg');
INSERT INTO Artists (name, year_formed, active, image_url) VALUES ('Triumph', '1975', false, 'https://upload.wikimedia.org/wikipedia/commons/thumb/9/9d/Triumph_at_sweden_rock%2C_2008.JPG/220px-Triumph_at_sweden_rock%2C_2008.JPG');
INSERT INTO Artists (name, year_formed, active, image_url) VALUES ('Van Halen', '1972', false, 'https://upload.wikimedia.org/wikipedia/commons/thumb/8/86/Van_Halen_2008_%28crop%29.jpg/300px-Van_Halen_2008_%28crop%29.jpg');
INSERT INTO Artists (name, year_formed, active, image_url) VALUES ('Yes', '1968', true, 'https://upload.wikimedia.org/wikipedia/commons/thumb/9/93/Yes_concert.jpg/296px-Yes_concert.jpg');
```

These SQL statements will populate the following `ArtistDto` object:

```
@Data
public class ArtistDto {
    private String name;
    private String yearFormed;
    private boolean active;
    private String imageUrl;
    private double rating;
}
```

## API Configuration Information

The following element needs to be set within Spring Boot in order to run the `slash-graph-ql-angular` application:

* `${SLASH_GRAPH_QL_HOSTNAME}` - GraphQL Endpoint (hostname) as shown in the Dgraph GraphQL Dashboard `[slash-graph-ql.hostname]`

## Starting the Angular Client

The Angular client is already configured to run on port `4200` (by default) and is configured to access the API using (default) Spring Boot port `8080`.  As a result, no changes should be required to prepare the Angular client for use.

## Using This Repository

Follow the short steps below to utilize this repository:

1. Validate API configuration, including setting of the `[slash-graph-ql.hostname]` property
1. Start the Spring Boot service
1. Validate the Angular client configuration, change the `environment` folder's files if the Spring Boot port has changed
1. Start the Angular client using `ng serve`
1. Launch the (http://localhost:4200) URL to access the Angular client

## Using the Angular Application

Upon launching the Angular application (http://localhost:4200), the following screen should appear:

![Artists List](./ArtistsList.png)

This screen provides a summary of the artists configured in both Dgraph Slash GraphQL and the H2 database within Spring Boot.  

Single-clicking one of the entries will display information similar to what is shown below:

![Artist View (Russell)](./RushRecommendationRussell.png)

In this example, the `Russell` customer has been selected.  If the `Stacy` customer is selected instead, the screen should be updated as shown below:

![Artist View (Russell)](./RushRecommendationStacy.png)

This indicates that values provided by the current Artist and selected Customer lead to slightly different recommendations.

## Getting a Random Recommendation (Using the API)

In order to use the Dgraph Slash GraphQL Spring Boot service and data in GraphQL to make a recommendation, simply call the following cURL:
 
`curl --location --request GET 'http://localhost:8080/recommend'`

Based upon the matrix of data provided, results similar to those noted below will be returned:

```
{
    "matchedCustomer": {
        "username": "Russell"
    },
    "recommendations": [
        {
            "name": "Eric Johnson",
            "yearFormed": "1969",
            "active": true,
            "imageUrl": "https://upload.wikimedia.org/wikipedia/commons/thumb/7/7e/Eric_Johnson_2017_%C2%A9John_Bland.jpg/220px-Eric_Johnson_2017_%C2%A9John_Bland.jpg",
            "rating": 4.5,
            "score": 0.827008642353265
        },
        {
            "name": "Journey",
            "yearFormed": "1973",
            "active": true,
            "imageUrl": "https://upload.wikimedia.org/wikipedia/commons/thumb/b/be/Journey_publicity_photo_2013.jpg/220px-Journey_publicity_photo_2013.jpg",
            "rating": 4.0,
            "score": 0.8255605295594078
        },
        {
            "name": "Yes",
            "yearFormed": "1968",
            "active": true,
            "imageUrl": "https://upload.wikimedia.org/wikipedia/commons/thumb/9/93/Yes_concert.jpg/296px-Yes_concert.jpg",
            "rating": 4.3,
            "score": 0.7723577683378303
        },
        {
            "name": "Night Ranger",
            "yearFormed": "1982",
            "active": true,
            "imageUrl": "https://upload.wikimedia.org/wikipedia/commons/thumb/1/1d/Night_Ranger_live.jpg/300px-Night_Ranger_live.jpg",
            "rating": 2.5,
            "score": 0.7143509801901626
        },
        {
            "name": "Triumph",
            "yearFormed": "1975",
            "active": false,
            "imageUrl": "https://upload.wikimedia.org/wikipedia/commons/thumb/9/9d/Triumph_at_sweden_rock%2C_2008.JPG/220px-Triumph_at_sweden_rock%2C_2008.JPG",
            "rating": 3.5,
            "score": 0.7131612507922729
        }
    ],
    "ratingsMap": {
        "GraphQlArtist(name=Led Zeppelin, ratings=null)": 1.0,
        "GraphQlArtist(name=Rush, ratings=null)": 0.6,
        "GraphQlArtist(name=Van Halen, ratings=null)": 0.8,
        "GraphQlArtist(name=Tool, ratings=null)": 1.0,
        "GraphQlArtist(name=Genesis, ratings=null)": 0.4
    },
    "resultsMap": {
        "GraphQlArtist(name=Led Zeppelin, ratings=null)": 1.0,
        "GraphQlArtist(name=Triumph, ratings=null)": 0.7131612507922729,
        "GraphQlArtist(name=Rush, ratings=null)": 0.6,
        "GraphQlArtist(name=Eric Johnson, ratings=null)": 0.827008642353265,
        "GraphQlArtist(name=Van Halen, ratings=null)": 0.8,
        "GraphQlArtist(name=Journey, ratings=null)": 0.8255605295594078,
        "GraphQlArtist(name=Night Ranger, ratings=null)": 0.7143509801901626,
        "GraphQlArtist(name=Tool, ratings=null)": 1.0,
        "GraphQlArtist(name=Genesis, ratings=null)": 0.4,
        "GraphQlArtist(name=Yes, ratings=null)": 0.7723577683378303
    }
}
```

where:

* `matchedCustomer` is a random customer selected (using the [`random-generator`](https://gitlab.com/johnjvester/RandomGenerator) library) to match the current user
* `ratingsMap` is a summary of the ratings provided by that random customer
* `resultsMap` contains the results from the Slope One algorithm being populated for items not ranked by the random customer selected
* `recommendationsMap` is an update copy of the `resultsMap` without the `ratingsMap` data, providing recommendations (and their Slope One weight) for the random customer using the `ArtistDto` as the object type

What this means is that a customer similar to `Russell` would most likely enjoy the music of `Eric Johnson`, based upon Russell's provided reviews and the computed reviews based upon the results of other customers.  Of course this is a very simple example of a recommendations engine - merely to illustrate how data in Dgraph GraphQL can be gathered and processed.

## Getting a Recommendation for a Specified User and a Provided Artist (Using the API)

In order to use the Dgraph Slash GraphQL Spring Boot AOU and data in GraphQL to make a recommendation for a specified user and a provided artist, simply call the following cURL:

```
curl --location --request PUT 'http://localhost:8080/recommend/Russell' \
--header 'Content-Type: application/json' \
--data-raw '{
    "name": "Eric Johnson"
}'
```
 
In this example, the `Russell` user is currently viewing the music by `Eric Johnson`, so the corresponding payload would appear as shown below:

```
{
    "matchedCustomer": {
        "username": "Russell"
    },
    "recommendations": [
        {
            "name": "Journey",
            "yearFormed": "1973",
            "active": true,
            "imageUrl": "https://upload.wikimedia.org/wikipedia/commons/thumb/b/be/Journey_publicity_photo_2013.jpg/220px-Journey_publicity_photo_2013.jpg",
            "rating": 4.0,
            "score": 0.8258367589987565
        },
        {
            "name": "Yes",
            "yearFormed": "1968",
            "active": true,
            "imageUrl": "https://upload.wikimedia.org/wikipedia/commons/thumb/9/93/Yes_concert.jpg/296px-Yes_concert.jpg",
            "rating": 4.3,
            "score": 0.7714146566949098
        },
        {
            "name": "Night Ranger",
            "yearFormed": "1982",
            "active": true,
            "imageUrl": "https://upload.wikimedia.org/wikipedia/commons/thumb/1/1d/Night_Ranger_live.jpg/300px-Night_Ranger_live.jpg",
            "rating": 2.5,
            "score": 0.7190513335939437
        },
        {
            "name": "Triumph",
            "yearFormed": "1975",
            "active": false,
            "imageUrl": "https://upload.wikimedia.org/wikipedia/commons/thumb/9/9d/Triumph_at_sweden_rock%2C_2008.JPG/220px-Triumph_at_sweden_rock%2C_2008.JPG",
            "rating": 3.5,
            "score": 0.7145719024054821
        }
    ],
    "ratingsMap": {
        "GraphQlArtist(name=Led Zeppelin, ratings=null)": 1.0,
        "GraphQlArtist(name=Rush, ratings=null)": 0.6,
        "GraphQlArtist(name=Van Halen, ratings=null)": 0.8,
        "GraphQlArtist(name=Tool, ratings=null)": 1.0,
        "GraphQlArtist(name=Genesis, ratings=null)": 0.4
    },
    "resultsMap": {
        "GraphQlArtist(name=Led Zeppelin, ratings=null)": 1.0,
        "GraphQlArtist(name=Triumph, ratings=null)": 0.7145719024054821,
        "GraphQlArtist(name=Rush, ratings=null)": 0.6,
        "GraphQlArtist(name=Eric Johnson, ratings=null)": 0.8245936484242476,
        "GraphQlArtist(name=Van Halen, ratings=null)": 0.8,
        "GraphQlArtist(name=Journey, ratings=null)": 0.8258367589987565,
        "GraphQlArtist(name=Night Ranger, ratings=null)": 0.7190513335939437,
        "GraphQlArtist(name=Tool, ratings=null)": 1.0,
        "GraphQlArtist(name=Genesis, ratings=null)": 0.4,
        "GraphQlArtist(name=Yes, ratings=null)": 0.7714146566949098
    }
}
```

The difference with this approach, is that `Eric Johnson` would not be included in the recommended list.


Made with ♥ by johnjvester@gmail.com, because I enjoy writing code.