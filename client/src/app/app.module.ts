import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { ViewArtistComponent } from './view-artist/view-artist.component';
import {BooleanPipe} from './utils/boolean.pipe';
import {ArtistService} from './services/artist.service';
import {HttpClientModule} from '@angular/common/http';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { ListArtistsComponent } from './list-artists/list-artists.component';
import {RouterModule} from '@angular/router';
import {StarRatingModule} from 'angular-star-rating';
import {RecommendationService} from './services/recommendation.service';
import {CustomerService} from './services/customer.service';
import {FormsModule} from '@angular/forms';

@NgModule({
  declarations: [
    AppComponent,
    BooleanPipe,
    ViewArtistComponent,
    ListArtistsComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    NgbModule,
    RouterModule.forRoot([
      {path: '', redirectTo: '/list-artists', pathMatch: 'full'},
      {path: 'list-artists', component: ListArtistsComponent},
      {path: 'view-artist', component: ViewArtistComponent},
    ]),
    StarRatingModule.forRoot(),
    FormsModule
  ],
  providers: [
    ArtistService,
    CustomerService,
    RecommendationService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
