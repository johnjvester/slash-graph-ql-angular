import { Component, OnInit } from '@angular/core';
import {ArtistDto} from '../models/artist-dto.model';
import {Router} from '@angular/router';
import {ArtistService} from '../services/artist.service';

@Component({
  selector: 'app-list-artists',
  templateUrl: './list-artists.component.html',
  styleUrls: ['./list-artists.component.css']
})
export class ListArtistsComponent implements OnInit {
  artists: ArtistDto[];

  constructor(public router: Router, private artistsService: ArtistService) { }

  ngOnInit() {
    this.artistsService.getArtists()
      .subscribe(data => {
        this.artists = data;
      }, (error) => {
        console.error('error', error);
      });
  }

  showArtist(thisArtist: ArtistDto) {
    localStorage.removeItem('artist');
    localStorage.setItem('artist', JSON.stringify(thisArtist));
    this.router.navigate(['view-artist']);
  }
}
