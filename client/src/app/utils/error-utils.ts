import {HttpErrorResponse} from '@angular/common/http';
import {throwError} from 'rxjs';

export default class ErrorUtils {

  static errorHandler(error: HttpErrorResponse) {
    let thisErrorMessage;

    if (error.error instanceof ErrorEvent) {
      thisErrorMessage = `Error: ${error.error.message}`;
    } else {
      thisErrorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }

    console.log(thisErrorMessage);
    return throwError(`${error.status} error`);
  }
}
