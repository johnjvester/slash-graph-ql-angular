import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../environments/environment';
import {ArtistDto} from '../models/artist-dto.model';
import {catchError, tap} from 'rxjs/operators';
import ErrorUtils from '../utils/error-utils';

@Injectable()
export class ArtistService {

  constructor(private http: HttpClient) { }
  baseUrl: string = environment.api + '/artists';

  getArtists() {
    return this.http.get<ArtistDto[]>(this.baseUrl).pipe(
      tap(),
      catchError(err => { return ErrorUtils.errorHandler(err)
      }));
  }
}
