import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../environments/environment';
import {ArtistDto} from '../models/artist-dto.model';
import {catchError, tap} from 'rxjs/operators';
import ErrorUtils from '../utils/error-utils';
import {Recommendation} from '../models/recommendation.model';

@Injectable()
export class RecommendationService {

  constructor(private http: HttpClient) { }
  baseUrl: string = environment.api + '/recommend';

  makeRecommendation(username: string, artist: ArtistDto) {
    return this.http.put<Recommendation>(this.baseUrl  + '/' + username, artist).pipe(
      tap(),
      catchError(err => { return ErrorUtils.errorHandler(err)
      }));
  }
}
