import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../environments/environment';
import {catchError, tap} from 'rxjs/operators';
import ErrorUtils from '../utils/error-utils';
import {Customer} from '../models/customer.model';

@Injectable()
export class CustomerService {

  constructor(private http: HttpClient) { }
  baseUrl: string = environment.api + '/customers';

  getCustomers() {
    return this.http.get<Customer[]>(this.baseUrl).pipe(
      tap(),
      catchError(err => { return ErrorUtils.errorHandler(err)
      }));
  }
}
