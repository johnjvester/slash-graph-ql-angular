import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import {ArtistDto} from '../models/artist-dto.model';
import {CustomerService} from '../services/customer.service';
import {RecommendationService} from '../services/recommendation.service';
import {Customer} from '../models/customer.model';
import {Recommendation} from '../models/recommendation.model';

@Component({
  selector: 'app-view-artist',
  templateUrl: './view-artist.component.html',
  styleUrls: ['./view-artist.component.css']
})
export class ViewArtistComponent implements OnInit {
  artist: ArtistDto;
  customer: Customer;
  customers: Customer[];
  recommendation: Recommendation;

  constructor(public router: Router, private customerService: CustomerService, private recommendationService: RecommendationService) { }

  ngOnInit() {
    let artistData = localStorage.getItem('artist');
    if ((artistData)) {
      this.artist = JSON.parse(artistData);
      this.customerService.getCustomers()
        .subscribe(data => {
          this.customers = data;
          this.customer = data[0];
          this.getRecommendationData();
        }, (error) => {
          console.error('error', error);
        });
    } else {
      this.returnToView();
    }
  }

  returnToView() {
    this.router.navigate(['list-artists']);
  }

  getRecommendationData() {
    this.recommendationService.makeRecommendation(this.customer.username, this.artist)
      .subscribe(data => {
        this.recommendation = data;
      }, (error) => {
        console.error('error', error);
      });
  }

}
