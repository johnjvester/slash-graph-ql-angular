export class ArtistDto {
  name: string;
  yearFormed: string;
  active: boolean;
  imageUrl: string;
  rating: number;
  score: number;
}
