import {ArtistDto} from './artist-dto.model';
import {Customer} from './customer.model';

export class Recommendation {
  matchedCustomer: Customer;
  recommendations: ArtistDto[];
}
