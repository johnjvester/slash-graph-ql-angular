package com.gitlab.johnjvester.slash.models.graphQL;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.gitlab.johnjvester.slash.models.Customer;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@JsonIgnoreProperties
@AllArgsConstructor
@NoArgsConstructor
@Data
public class GraphQlRating {
    private String id;
    private double score;
    private Customer by;
    private GraphQlArtist about;
}
