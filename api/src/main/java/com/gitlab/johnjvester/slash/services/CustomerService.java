package com.gitlab.johnjvester.slash.services;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.gitlab.johnjvester.slash.config.SlashGraphQlProperties;
import com.gitlab.johnjvester.slash.models.Customer;
import com.gitlab.johnjvester.slash.models.graphQL.GraphQlDataGetCustomer;
import com.gitlab.johnjvester.slash.models.graphQL.SlashGraphQlResultGetCustomer;
import com.gitlab.johnjvester.slash.models.graphQL.SlashGraphQlResultQueryCustomer;
import com.gitlab.johnjvester.slash.utils.RestTemplateUtils;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;

@Slf4j
@RequiredArgsConstructor
@Service
public class CustomerService {
    private final SlashGraphQlProperties slashGraphQlProperties;

    private static final String CUSTOMER_QUERY = "query { queryCustomer(order: {asc: username}) { username } }";
    private static final String CUSTOMER_QUERY_PREFIX = "query { getCustomer(username: \"";
    private static final String CUSTOMER_QUERY_SUFFIX = "\") { username } }";

    public List<Customer> getCustomers() throws Exception {
        ResponseEntity<String> responseEntity = RestTemplateUtils.query(slashGraphQlProperties.getHostname(), CUSTOMER_QUERY);

        try {
            ObjectMapper objectMapper = new ObjectMapper();
            SlashGraphQlResultQueryCustomer slashGraphQlResult = objectMapper.readValue(responseEntity.getBody(), SlashGraphQlResultQueryCustomer.class);
            log.debug("slashGraphQlResult={}", slashGraphQlResult);
            return slashGraphQlResult.getData().getQueryCustomer();
        } catch (JsonProcessingException e) {
            throw new Exception("An error was encountered processing responseEntity=" + responseEntity.getBody(), e);
        }
    }

    public Customer getCustomer(String username) throws Exception {
        ResponseEntity<String> responseEntity = RestTemplateUtils.query(slashGraphQlProperties.getHostname(), CUSTOMER_QUERY_PREFIX + username + CUSTOMER_QUERY_SUFFIX);

        try {
            ObjectMapper objectMapper = new ObjectMapper();
            SlashGraphQlResultGetCustomer slashGraphQlResult = objectMapper.readValue(responseEntity.getBody(), SlashGraphQlResultGetCustomer.class);
            log.debug("slashGraphQlResult={}", slashGraphQlResult);
            GraphQlDataGetCustomer graphQlDataGetCustomer = slashGraphQlResult.getData();

            if (graphQlDataGetCustomer != null) {
                return graphQlDataGetCustomer.getGetCustomer();
            }
        } catch (JsonProcessingException e) {
            throw new Exception("An error was encountered processing responseEntity=" + responseEntity.getBody(), e);
        }

        return null;
    }
}
