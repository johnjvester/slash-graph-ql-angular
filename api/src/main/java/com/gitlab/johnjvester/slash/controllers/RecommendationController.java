package com.gitlab.johnjvester.slash.controllers;

import com.gitlab.johnjvester.slash.models.ArtistDto;
import com.gitlab.johnjvester.slash.models.Recommendation;
import com.gitlab.johnjvester.slash.services.RecommendationService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

@Slf4j
@RequiredArgsConstructor
@CrossOrigin
@Controller
@RequestMapping(produces = MediaType.APPLICATION_JSON_VALUE)
public class RecommendationController {
    private final RecommendationService recommendationService;

    @GetMapping(value = "/recommend")
    public ResponseEntity<Recommendation> recommend() {
        try {
            return new ResponseEntity<>(recommendationService.recommend(), HttpStatus.OK);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    @PutMapping(value = "/recommend/{username}")
    public ResponseEntity<Recommendation> recommend(@PathVariable String username, @RequestBody(required = false) ArtistDto artistDto) {
        try {
            return new ResponseEntity<>(recommendationService.recommend(username, artistDto), HttpStatus.ACCEPTED);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }
}
