package com.gitlab.johnjvester.slash.entities;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity
@Table(name = "artists")
public class Artist {
    @Id
    private String name;
    private String yearFormed;
    private boolean active;
    private String imageUrl;
}
