package com.gitlab.johnjvester.slash.repositories;

import com.gitlab.johnjvester.slash.entities.Artist;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ArtistRepository extends JpaRepository<Artist, String> { }
