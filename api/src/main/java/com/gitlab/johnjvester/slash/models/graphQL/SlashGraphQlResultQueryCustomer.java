package com.gitlab.johnjvester.slash.models.graphQL;

import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@Data
public class SlashGraphQlResultQueryCustomer {
    private GraphQlDataQueryCustomer data;
    private Object extensions;
}
