package com.gitlab.johnjvester.slash.controllers;

import com.gitlab.johnjvester.slash.models.ArtistDto;
import com.gitlab.johnjvester.slash.services.ArtistService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@Slf4j
@RequiredArgsConstructor
@CrossOrigin
@Controller
@RequestMapping(produces = MediaType.APPLICATION_JSON_VALUE)
public class ArtistController {
    private final ArtistService artistService;

    @GetMapping(value = "/artists")
    public ResponseEntity<List<ArtistDto>> getArtists() {
        try {
            return new ResponseEntity<>(artistService.getArtists(), HttpStatus.OK);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }
}
