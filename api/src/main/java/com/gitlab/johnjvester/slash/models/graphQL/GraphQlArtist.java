package com.gitlab.johnjvester.slash.models.graphQL;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@JsonIgnoreProperties
@AllArgsConstructor
@NoArgsConstructor
@Data
public class GraphQlArtist {
    public String name;
    public List<GraphQlRating> ratings;
}
