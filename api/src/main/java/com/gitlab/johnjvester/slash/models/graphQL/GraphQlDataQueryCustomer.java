package com.gitlab.johnjvester.slash.models.graphQL;

import com.fasterxml.jackson.annotation.JsonRootName;
import com.gitlab.johnjvester.slash.models.Customer;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@JsonRootName(value = "data")
@AllArgsConstructor
@NoArgsConstructor
@Data
public class GraphQlDataQueryCustomer {
    private List<Customer> queryCustomer;
}
