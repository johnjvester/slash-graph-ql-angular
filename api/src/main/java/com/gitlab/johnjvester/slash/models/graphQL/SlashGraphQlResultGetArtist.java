package com.gitlab.johnjvester.slash.models.graphQL;

import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@Data
public class SlashGraphQlResultGetArtist {
    private GraphQlDataGetArtist data;
    private Object extensions;
}
