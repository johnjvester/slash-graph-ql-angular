package com.gitlab.johnjvester.slash.services;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.gitlab.johnjvester.randomizer.RandomGenerator;
import com.gitlab.johnjvester.slash.config.SlashGraphQlProperties;
import com.gitlab.johnjvester.slash.engine.SlopeOne;
import com.gitlab.johnjvester.slash.models.ArtistDto;
import com.gitlab.johnjvester.slash.models.ArtistDtoRecommendation;
import com.gitlab.johnjvester.slash.models.Customer;
import com.gitlab.johnjvester.slash.models.Recommendation;
import com.gitlab.johnjvester.slash.models.graphQL.GraphQlArtist;
import com.gitlab.johnjvester.slash.models.graphQL.GraphQlDataRating;
import com.gitlab.johnjvester.slash.models.graphQL.GraphQlRating;
import com.gitlab.johnjvester.slash.models.graphQL.SlashGraphQlResultRating;
import com.gitlab.johnjvester.slash.utils.RestTemplateUtils;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Slf4j
@RequiredArgsConstructor
@Service
public class RecommendationService {
    private final ArtistService artistService;
    private final CustomerService customerService;
    private final SlashGraphQlProperties slashGraphQlProperties;

    private static final String RATING_QUERY = "query RatingQuery { queryRating { id, score, by { username }, about { name } } }";

    public Recommendation recommend() throws Exception {
        return handleRecommendation(null, null);
    }

    public Recommendation recommend(String username, ArtistDto artistDto) throws Exception {
        return handleRecommendation(username, artistDto);
    }

    private Recommendation handleRecommendation(String username, ArtistDto artistDto) throws Exception {
        ResponseEntity<String> responseEntity = RestTemplateUtils.query(slashGraphQlProperties.getHostname(), RATING_QUERY);

        try {
            ObjectMapper objectMapper = new ObjectMapper();
            SlashGraphQlResultRating slashGraphQlResult = objectMapper.readValue(responseEntity.getBody(), SlashGraphQlResultRating.class);
            log.debug("slashGraphQlResult={}", slashGraphQlResult);

            return makeRecommendation(slashGraphQlResult.getData(), username, artistDto);
        } catch (JsonProcessingException e) {
            throw new Exception("An error was encountered processing responseEntity=" + responseEntity.getBody(), e);
        }
    }

    private Recommendation makeRecommendation(GraphQlDataRating ratings, String username, ArtistDto artistDto) throws Exception {
        if (ratings == null || CollectionUtils.isEmpty(ratings.getQueryRating())) {
            throw new Exception("No ratings found to process");
        }

        Map<Customer, HashMap<GraphQlArtist, Double>> data = new HashMap<>();

        for (GraphQlRating rating : ratings.getQueryRating()) {
            if (data.containsKey(rating.getBy())) {
                if (!data.get(rating.getBy()).containsKey(rating.getAbout())) {
                    data.get(rating.getBy()).put(rating.getAbout(), computeRating(rating.getScore()));
                } else {
                    log.warn("Multiple ratings found skipping");
                }
            } else {
                HashMap<GraphQlArtist, Double> customerRating = new HashMap<>();
                customerRating.put(rating.getAbout(), computeRating(rating.getScore()));
                data.put(rating.getBy(), customerRating);
            }
        }

        log.debug("data={}", data);

        Map<Customer, HashMap<GraphQlArtist, Double>> projectedData = SlopeOne.slopeOne(data, artistService.getAllArtists());

        Customer customer;

        if (StringUtils.isNotEmpty(username)) {
            customer = customerService.getCustomer(username);
        } else {
            log.info("Using RandomGenerator to locate a customer");
            RandomGenerator<Customer> randomGenerator = new RandomGenerator<>();
            List<Customer> randomCustomers = randomGenerator.randomize(customerService.getCustomers(), new Integer("1"));
            customer = randomCustomers.get(0);
        }

        log.info("customer={}", customer);

        Recommendation recommendation = new Recommendation();
        recommendation.setMatchedCustomer(customer);
        recommendation.setResultsMap(projectedData.get(recommendation.getMatchedCustomer()));
        recommendation.setRatingsMap(data.get(recommendation.getMatchedCustomer()));
        recommendation.setRecommendations(createRecommendationsList(recommendation.getRatingsMap(), recommendation.getResultsMap(), artistDto));

        return recommendation;
    }

    private List<ArtistDtoRecommendation> createRecommendationsList(HashMap<GraphQlArtist, Double> ratingsMap, HashMap<GraphQlArtist, Double> resultsMap, ArtistDto artistDto) throws Exception {
        List<ArtistDtoRecommendation> recommendations = new ArrayList<>();

        for (Map.Entry<GraphQlArtist, Double> pair : resultsMap.entrySet()) {
            if (!hasRatedThisArtist(ratingsMap, pair.getKey(), pair.getValue()) && (artistDto == null || !pair.getKey().getName().equals(artistDto.getName()))) {
                ArtistDtoRecommendation recommendation = artistService.getArtistRecommendation(pair.getKey());
                recommendation.setScore(pair.getValue());
                recommendations.add(recommendation);
            }
        }

        recommendations.sort(Comparator.comparing(ArtistDtoRecommendation::getScore).reversed());
        return recommendations;
    }

    private boolean hasRatedThisArtist(HashMap<GraphQlArtist, Double> ratingsMap, GraphQlArtist artistDto, Double value) {
        if (MapUtils.isNotEmpty(ratingsMap)) {
            for (Map.Entry<GraphQlArtist, Double> pair : ratingsMap.entrySet()) {
                if (pair.getKey().getName().equals(artistDto.getName()) && pair.getValue().equals(value)) {
                    return true;
                }
            }
        }

        return false;
    }

    /**
     * Simple example: converts a (double) score which should have a range of 0 - 5 into a (double) range from 0.0 - 1.0.
     *
     * @param score (double) score to analyze
     * @return (double)
     */
    private double computeRating(double score) {
        switch ((int) score) {
            case 5 : return 1.0d;
            case 4 : return 0.8d;
            case 3 : return 0.6d;
            case 2 : return 0.4d;
            case 1 : return 0.2d;
            case 0 :
            default:
                return 0.0d;
        }
    }
}
