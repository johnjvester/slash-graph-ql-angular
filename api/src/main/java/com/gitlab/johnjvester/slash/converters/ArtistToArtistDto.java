package com.gitlab.johnjvester.slash.converters;

import com.gitlab.johnjvester.slash.entities.Artist;
import com.gitlab.johnjvester.slash.models.ArtistDto;
import org.springframework.beans.BeanUtils;

public final class ArtistToArtistDto {
    private ArtistToArtistDto() { }

    public static ArtistDto convert(Artist artist) {
        ArtistDto artistDto = new ArtistDto();
        BeanUtils.copyProperties(artist, artistDto);
        return artistDto;
    }
}
