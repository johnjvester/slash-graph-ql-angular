package com.gitlab.johnjvester.slash.models;

import com.gitlab.johnjvester.slash.models.graphQL.GraphQlArtist;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.HashMap;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class Recommendation {
    private Customer matchedCustomer;
    private List<ArtistDtoRecommendation> recommendations;
    private HashMap<GraphQlArtist, Double> ratingsMap;
    private HashMap<GraphQlArtist, Double> resultsMap;
}
