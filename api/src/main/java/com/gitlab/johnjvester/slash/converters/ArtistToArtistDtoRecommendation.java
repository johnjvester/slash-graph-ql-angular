package com.gitlab.johnjvester.slash.converters;

import com.gitlab.johnjvester.slash.entities.Artist;
import com.gitlab.johnjvester.slash.models.ArtistDtoRecommendation;
import org.springframework.beans.BeanUtils;

public final class ArtistToArtistDtoRecommendation {
    private ArtistToArtistDtoRecommendation() { }

    public static ArtistDtoRecommendation convert(Artist artist) {
        ArtistDtoRecommendation recommendation = new ArtistDtoRecommendation();
        BeanUtils.copyProperties(artist, recommendation);
        return recommendation;
    }
}
