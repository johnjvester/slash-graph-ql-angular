package com.gitlab.johnjvester.slash.models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class ArtistDto {
    private String name;
    private String yearFormed;
    private boolean active;
    private String imageUrl;
    private double rating;
}
