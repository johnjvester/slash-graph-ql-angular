package com.gitlab.johnjvester.slash.services;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.gitlab.johnjvester.slash.config.SlashGraphQlProperties;
import com.gitlab.johnjvester.slash.converters.ArtistToArtistDto;
import com.gitlab.johnjvester.slash.converters.ArtistToArtistDtoRecommendation;
import com.gitlab.johnjvester.slash.entities.Artist;
import com.gitlab.johnjvester.slash.models.ArtistDto;
import com.gitlab.johnjvester.slash.models.ArtistDtoRecommendation;
import com.gitlab.johnjvester.slash.models.graphQL.GraphQlArtist;
import com.gitlab.johnjvester.slash.models.graphQL.GraphQlDataGetArtist;
import com.gitlab.johnjvester.slash.models.graphQL.GraphQlRating;
import com.gitlab.johnjvester.slash.models.graphQL.SlashGraphQlResultGetArtist;
import com.gitlab.johnjvester.slash.models.graphQL.SlashGraphQlResultQueryArtist;
import com.gitlab.johnjvester.slash.repositories.ArtistRepository;
import com.gitlab.johnjvester.slash.utils.RestTemplateUtils;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Slf4j
@RequiredArgsConstructor
@Service
public class ArtistService {
    private final ArtistRepository artistRepository;
    private final SlashGraphQlProperties slashGraphQlProperties;

    private static final String ARTIST_QUERY = "query { queryArtist(order: {asc: name}) { name } }";
    private static final String ARTIST_RATINGS_QUERY_PREFIX = "query { getArtist(name: \"";
    private static final String ARTIST_RATINGS_QUERY_SUFFIX = "\") { name, ratings { score } } }";
    private static final DecimalFormat df2 = new DecimalFormat("#.#");

    public List<ArtistDto> getArtists() throws Exception {
        List<ArtistDto> artistDtoList = new ArrayList<>();

        List<Artist> artists = artistRepository.findAll();

        if (CollectionUtils.isNotEmpty(artists)) {
            for (Artist artist : artists) {
                log.debug("artist={}", artist);
                ArtistDto artistDto = ArtistToArtistDto.convert(artist);
                determineAverageRating(artistDto);
                artistDtoList.add(artistDto);
            }
        }

        return artistDtoList;
    }

    public ArtistDtoRecommendation getArtistRecommendation(GraphQlArtist graphQlArtist) throws Exception {
        ArtistDtoRecommendation recommendation = new ArtistDtoRecommendation();
        recommendation.setName(graphQlArtist.getName());

        Optional<Artist> optional = artistRepository.findById(graphQlArtist.getName());

        if (optional.isPresent()) {
            recommendation = ArtistToArtistDtoRecommendation.convert(optional.get());
            determineAverageRating(recommendation);
        }

        return recommendation;
    }

    private void determineAverageRating(ArtistDto artistDto) throws Exception {
        ResponseEntity<String> responseEntity = RestTemplateUtils.query(slashGraphQlProperties.getHostname(), ARTIST_RATINGS_QUERY_PREFIX + artistDto.getName() + ARTIST_RATINGS_QUERY_SUFFIX);

        try {
            ObjectMapper objectMapper = new ObjectMapper();
            SlashGraphQlResultGetArtist slashGraphQlResult = objectMapper.readValue(responseEntity.getBody(), SlashGraphQlResultGetArtist.class);
            log.debug("slashGraphQlResult={}", slashGraphQlResult);
            GraphQlDataGetArtist graphQlDataGetArtist = slashGraphQlResult.getData();

            if (graphQlDataGetArtist != null) {
                GraphQlArtist graphQlArtist = graphQlDataGetArtist.getGetArtist();

                if (CollectionUtils.isNotEmpty(graphQlArtist.getRatings())) {
                    artistDto.setRating(Double.parseDouble(df2.format(graphQlArtist.getRatings().stream().mapToDouble(GraphQlRating::getScore).average().orElse(0.0))));
                }

            }
        } catch (JsonProcessingException e) {
            throw new Exception("An error was encountered processing responseEntity=" + responseEntity.getBody(), e);
        }
    }

    public List<GraphQlArtist> getAllArtists() throws Exception {
        ResponseEntity<String> responseEntity = RestTemplateUtils.query(slashGraphQlProperties.getHostname(), ARTIST_QUERY);

        try {
            ObjectMapper objectMapper = new ObjectMapper();
            SlashGraphQlResultQueryArtist slashGraphQlResult = objectMapper.readValue(responseEntity.getBody(), SlashGraphQlResultQueryArtist.class);
            log.debug("slashGraphQlResult={}", slashGraphQlResult);
            return slashGraphQlResult.getData().getQueryArtist();
        } catch (JsonProcessingException e) {
            throw new Exception("An error was encountered processing responseEntity=" + responseEntity.getBody(), e);
        }
    }
}
